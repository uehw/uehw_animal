﻿#include "Animal.h"
#include "Dog.h"
#include "Cat.h"
#include "Cow.h"
#include "Pig.h"

int main()
{
    Animal* animals[4];

    animals[0] = new Dog;
    animals[1] = new Cat;
    animals[2] = new Cow;
    animals[3] = new Pig;

    for (int i = 0; i < 4; i++) 
    {
        animals[i]->Voice();
    }

    return 0;
}
