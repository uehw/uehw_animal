#pragma once

#include <string>
#include <iostream>

class Animal
{
public:
	Animal();
	virtual void Voice();
};